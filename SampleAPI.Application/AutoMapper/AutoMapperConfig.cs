﻿using AutoMapper;

namespace SampleAPI.Application.AutoMapper
{
    public static class AutoMapperConfig
    {
        public static MapperConfiguration RegisterMappings()
        {
            return new MapperConfiguration(cfg => { cfg.AddProfile(new DtoToDoMappingProfile()); });
        }
    }
}