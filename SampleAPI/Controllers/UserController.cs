﻿using Microsoft.AspNetCore.Mvc;

namespace SampleAPI.Controllers
{
    [ApiController]
    [ApiVersion("1.0")]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {
        [HttpGet("test")]
        public IActionResult Test()
        {
            return Ok();
        }
    }
}